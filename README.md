# archlinux_configs

Repository contains my desk environment tweaks/config files.

*  kwinrc : 
> ~/.config/kwinrc 
*  kwin.sh : 
> /etc/profile.d/kwin.sh 
*  xorg.conf : 
> /etc/X11/xorg.conf 